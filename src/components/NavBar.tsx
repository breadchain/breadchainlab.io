import React from 'react'
import BreadchainLogo from '../images/breadchain.inline.svg'
import { Link } from 'gatsby'

const DesktopNavBar = ({ children }) => (
  <div className="hidden sm:ml-6 sm:flex sm:space-x-8">{children}</div>
)

const DesktopNavBarLink = ({ href, text }) => {
  const selectedClassName =
    'inline-flex items-center px-1 pt-1 border-b-2 border-pink-500 text-sm font-medium text-gray-900'
  const nonSelectedClassName =
    'inline-flex items-center px-1 pt-1 border-b-2 border-transparent text-sm font-medium text-gray-500 hover:text-gray-700 hover:border-gray-300'

  return (
    <Link
      to={href}
      activeClassName={selectedClassName}
      className={nonSelectedClassName}
    >
      {text}
    </Link>
  )
}

const MobileNavBarLink = ({ href, text }) => {
  const selectedClassName =
    'block pl-3 pr-4 py-2 border-l-4 border-pink-500 text-base font-medium text-pink-700 bg-pink-50'

  const nonSelectedClassName =
    'block pl-3 pr-4 py-2 border-l-4 border-transparent text-base font-medium text-gray-600 hover:text-gray-800 hover:bg-gray-50 hover:border-gray-300'

  return (
    <Link
      to={href}
      activeClassName={selectedClassName}
      className={nonSelectedClassName}
    >
      {text}
    </Link>
  )
}

const MobileMenu = ({ mobileMenuIsOpen, children }) => (
  <div className={`${mobileMenuIsOpen ? 'block' : 'hidden'} sm:hidden`}>
    <div className="pt-2 pb-3 space-y-1">{children}</div>
  </div>
)

const MobileMenuButton = ({ toggleMobileMenu, mobileMenuIsOpen }) => (
  <div className="-mr-2 flex items-center sm:hidden">
    {/* Mobile menu button */}
    <button
      className="inline-flex items-center justify-center p-2 rounded-md text-gray-400 hover:text-gray-500 hover:bg-gray-100 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-pink-500"
      onClick={(_) => toggleMobileMenu()}
      aria-expanded="false"
    >
      <span className="sr-only">Open main menu</span>
      {/* Icon when menu is closed. */}
      {/*
          Heroicon name: menu

          Menu open: "hidden", Menu closed: "block"
        */}
      <svg
        className={`${mobileMenuIsOpen ? 'hidden' : 'block'} h-6 w-6`}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        aria-hidden="url"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M4 6h16M4 12h16M4 18h16"
        />
      </svg>
      {/* Icon when menu is open. */}
      {/*
          Heroicon name: x

          Menu open: "block", Menu closed: "hidden"
        */}
      <svg
        className={`${mobileMenuIsOpen ? 'block' : 'hidden'} h-6 w-6`}
        xmlns="http://www.w3.org/2000/svg"
        fill="none"
        viewBox="0 0 24 24"
        stroke="currentColor"
        aria-hidden="true"
      >
        <path
          strokeLinecap="round"
          strokeLinejoin="round"
          strokeWidth={2}
          d="M6 18L18 6M6 6l12 12"
        />
      </svg>
    </button>
  </div>
)

const NavBar = () => {
  const [mobileMenuIsOpen, setMobileMenuIsOpen] = React.useState(false)

  const toggleMobileMenu = () => {
    setMobileMenuIsOpen((value) => !value)
  }

  return (
    <nav className="bg-white shadow">
      <div className="max-w-7xl mx-auto px-4 sm:px-6 lg:px-8">
        <div className="flex justify-between h-16">
          <div className="flex">
            <div className="flex-shrink-0 flex items-center">
              <a href="/">
                <BreadchainLogo style={{ width: 96, }} />
              </a>
            </div>
          </div>
          <DesktopNavBar>
            <DesktopNavBarLink href="/about" text="About" />
            <DesktopNavBarLink href="/projects" text="Projects" />
            <DesktopNavBarLink href="/blog" text="Blog" />
          </DesktopNavBar>

          <MobileMenuButton
            mobileMenuIsOpen={mobileMenuIsOpen}
            toggleMobileMenu={toggleMobileMenu}
          />
        </div>
      </div>
      {/*
    Mobile menu, toggle classes based on menu state.

    Menu open: "block", Menu closed: "hidden"
  */}
      <MobileMenu mobileMenuIsOpen={mobileMenuIsOpen}>
        <MobileNavBarLink href="/about" text="About" />
        <MobileNavBarLink href="/projects" text="Projects" />
        <MobileNavBarLink href="/blog" text="Blog" />
      </MobileMenu>
    </nav>
  )
}

export default NavBar
