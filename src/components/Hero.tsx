import React from 'react'

const Hero = () => {
  return (
    <main className="lg:relative hero-bg">
      <div className="mx-auto max-w-7xl w-full pt-16 pb-20 text-center lg:py-48 lg:text-center">
        <div className="px-4 sm:px-8 xl:pr-16">
          <h1 className="text-4xl tracking-tight font-extrabold text-gray-50 sm:text-5xl md:text-6xl lg:text-5xl xl:text-6xl">
            <span className="block tracking-wider">BREADCHAIN</span>
            <span className="block text-pink-300 sm:text-4xl md:text-5xl lg:text-4xl xl:text-5xl">
              A community of decentralized, cooperatives projects
            </span>
          </h1>
          <p className="mt-3 max-w-md mx-auto text-lg text-gray-50 sm:text-xl md:mt-5 md:max-w-3xl">
            A place to forge solidarity through tech projects that help us build
            a truly cooperative, democratic economy.
          </p>
        </div>
      </div>
    </main>
  )
}

export default Hero
