import { graphql } from 'gatsby'
import React from 'react'
import Markdown from '../../components/Markdown'
import Layout from '../../components/layout'

export const query = graphql`
  query {
    allDirectusArticle {
      edges {
        node {
          directusId
          title
          body
        }
      }
    }
  }
`

const BlogsIndex = ({
  data: {
    allDirectusArticle: {
      edges: posts = []
    }
  }
}) => {
  const Card = ({ post: { directusId, title, body }}) => (
    <div className="h-48 w-72 flex flex-col rounded-lg shadow-lg overflow-hidden">
      <div className="flex-1 bg-white p-6 flex flex-col justify-between">
        <div className="flex-1">
          <a href={`/blog/${directusId}`} className="block">
            <h3 className="mt-2 text-xl leading-7 font-semibold text-gray-900">
              {title}
            </h3>
            <p className="mt-3 text-base leading-6 text-gray-500">
              <Markdown>
                {body}
              </Markdown>
            </p>
          </a>
        </div>
      </div>
    </div>


  )
  return (
    <Layout>
      <h1 className="mt-12 mx-auto text-left text-3xl font-bold">Blog Posts</h1>
      <div className={`relative max-w-7xl mx-auto`}>
        <div className={`mt-12 mb-12 grid gap-5 max-w-full mx-auto lg:grid-cols-3 lg:max-w-none`}>
          {posts.map(({ node: post }) => (
            <Card post={post} />
          ))}
        </div>
      </div>


    </Layout>
  )
}

export default BlogsIndex