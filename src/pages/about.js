import React from "react";

import Layout from "../components/layout";
import SEO from "../components/seo";

function AboutPage() {
  return (
    <Layout>
      <SEO
        keywords={[`gatsby`, `tailwind`, `react`, `tailwindcss`]}
        title="About"
      />

<section className="bg-gray-50">
        <div className="flex max-w-screen-xl mx-auto w-full justify-center py-12 px-4 sm:px-6 lg:py-16 lg:px-8">
          <article className="prose lg:prose-xl">
            <h1>About Us</h1>
            <p>
              Inspired by the successes of BreadTube, the loose association of leftist YouTubers working to fight against the right wing tilt on YouTube, Breadchain is a loose organization of blockchain or DLT (distributed ledger technologies) based projects that are working to advance a more progressive vision for this technology and its effect on society. The goal of this endeavor is to challenge the largely conservative and outdated economic models and assumptions in which many cryptocurrency and DLT projects are built upon (i.e., artificial scarcity in the form of capped supply, market-based incentive structures, an assumption of mistrust of all other actors, etc.) and demonstrate that an alternative framework is possible.
            </p>
            <p>
              In the blockchain and cryptocurrency space today, it is common to see new cryptocurrency projects raise millions of dollars in initial coin offerings (ICOs) without having any working product in which subsequently the founders are nowhere to be found and not held responsible due to lack of regulation, leaving investors and initial believers duped. A proper understanding of what causes this problem in the crypto space and other industries can be attributed to the nature of unrestricted capitalism, an economic system which prioritizes profit over all other economic and social measures.
            </p>
            <p>
              Investors have rightfully concluded that blockchain technology and cryptocurrency are novel innovations which have the potential to drastically shift how society could work. The problem with the introduction of novel technologies is that they are used in a way that maximizes the priorities of the dominant mode of production, today being capitalism. Because capitalism prioritizes increasing profits, especially when so unregulated, so do new technologies, including blockchain, rather than for the betterment of society. A good understanding of history will show that things which are profitable for the capitalist class are not necessarily beneficial for the rest of society. An economic system which prioritizes human need over financial gain would more efficiently allocate resources and rid of start-up attempts with faux promises for improving society.
            </p>

            <p>
              Breadchain seeks to take the particular parts of blockchain that have the potential to create a more fair society (i.e., peer to peer infrastructure, open source for anybody to use, ability to manipulate, and create new institutions, etc.) to design alternatives. For example, Basis, is an open source set of tools that seeks to enable a decentralized network of co-ops that eats capitalism over time, converting private resources into shared production based on use and need instead of profit allowing free association of producers and in-kind cost tracking as core concepts.          
            </p>

            <p>
              At the moment, Breadchain consists largely of a website which serves as the hub to easily find all associated projects. The future road map involves creating a comprehensive governance structure to add or remove associated projects and make BreadChain projects interoperable.
            </p>
            <p>
              The “Bread” in BreadChain refers to the book “The Conquest of Bread” by anarcho-communist Peter Kropotkin written in 1892 which discusses the issues with capitalism and alternatively how an anarcho-communist society would function. We are influenced by this book to guide the BreadChain project.
            </p>
          </article>
        </div>
      </section>
            
    </Layout>
  );
}

export default AboutPage;
