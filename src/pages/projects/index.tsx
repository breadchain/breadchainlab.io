import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../../components/layout'

export const query = graphql`
  query {
    allDirectusProject {
      edges {
        node {
          image {
            data {
              full_url
            }
          }
          directusId
          description
          title
          url
          tag
        }
      }
    }
  }
`

const Projects = ({
  data: {
    allDirectusProject: { edges: projects = [] },
  },
}) => {
  const Card = ({ project }) => (
    <div className="p-4 md:w-1/3">
      <div className="h-full shadow-lg rounded-lg overflow-hidden">
        <img src={project.image.data.full_url} />
        <div className="p-6">
          <h2 className="tracking-widest text-xs title-font font-medium text-gray-500 mb-1">
            {project.tag.toUpperCase()}
          </h2>
          <h1 className="title-font text-lg font-bold text-gray-900 mb-3">
            {project.title}
          </h1>
          <p className="leading-relaxed mb-3">{project.description}</p>
          <div className="flex items-center flex-wrap">
            <a
              href={project.url}
              className="text-pink-500 hover:text-pink-700 transition duration-150 ease-in-out inline-flex items-center md:mb-2 lg:mb-0"
            >
              Learn More
              <svg
                xmlns="http://www.w3.org/2000/svg"
                className="w-4 h-4 ml-2"
                viewBox="0 0 20 20"
                fill="currentColor"
              >
                <path
                  fillRule="evenodd"
                  d="M10.293 3.293a1 1 0 011.414 0l6 6a1 1 0 010 1.414l-6 6a1 1 0 01-1.414-1.414L14.586 11H3a1 1 0 110-2h11.586l-4.293-4.293a1 1 0 010-1.414z"
                  clipRule="evenodd"
                />
              </svg>
            </a>
          </div>
        </div>
      </div>
    </div>
  )

  return (
    <Layout>
      <h1 className="mt-12 mx-auto text-left text-3xl font-bold">
        Associated Projects
      </h1>
      {projects.map((project) => (
        <Card project={project.node} />
      ))}
    </Layout>
  )
}

export default Projects
