import React from 'react'
import { graphql } from 'gatsby'
import Layout from '../components/layout'
import Markdown from '../components/Markdown'

export default function BlogPost({ data }) {
  const post = data.directusArticle

  return (
    <Layout>
      <div className={`relative max-w-7xl mx-auto`}>
        <h1 className="title-font text-lg font-bold text-gray-900 mb-3">
          {post.title}
        </h1>

        <Markdown>{post.body}</Markdown>
      </div>
    </Layout>
  )
}

export const query = graphql`
  query BlogQuery($id: Int!) {
    directusArticle(directusId: { eq: $id }) {
      title
      body
    }
  }
`
