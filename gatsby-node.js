const path = require('path')

exports.createPages = async ({ graphql, actions }) => {
  const {createPage} = actions
  const result = await graphql(`
    query {
      allDirectusArticle {
        edges {
          node {
            directusId
            title
            body
          }
        }
      }
    }
  `)

  result.data.allDirectusArticle.edges.forEach(({ node }) => {
    createPage({
      path: `/blog/${node.directusId.toString()}`,
      component: path.resolve(`./src/templates/blog-post.js`),
      context: {
        id: node.directusId
      }
    })
  })

}